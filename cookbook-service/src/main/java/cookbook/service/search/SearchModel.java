package cookbook.service.search;

import java.util.List;

public interface SearchModel {
    String getText();
    List<String> getTypes();
}
