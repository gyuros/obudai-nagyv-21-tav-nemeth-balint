package cookbook.service.search;

import cookbook.service.dto.RecipeDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SearchService {

    public List<RecipeDto> filter(List<RecipeDto> recipes, SearchModel searchModel) {
        if(searchModel.getTypes() == null || searchModel.getText() == null)
            return recipes;

        return recipes.stream().filter(x -> {
            return (searchModel.getTypes().contains("name") && x.getName().toLowerCase().contains(searchModel.getText().toLowerCase())) ||
                    (searchModel.getTypes().contains("category") && x.getCategories().stream().anyMatch(y -> y.name().toLowerCase().contains(searchModel.getText().toLowerCase()))) ||
                    (searchModel.getTypes().contains("ingredient") && x.getIngredients().stream().anyMatch(y -> y.getName().toLowerCase().contains(searchModel.getText().toLowerCase()))) ||
                    (searchModel.getTypes().contains("uploader") && x.getUploader().getUsername().toLowerCase().contains(searchModel.getText().toLowerCase()));
        }).collect(Collectors.toList());
    }
}
