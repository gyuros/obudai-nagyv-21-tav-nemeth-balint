package cookbook.service.comment;

import cookbook.persistence.entity.Comment;
import cookbook.persistence.entity.Cook;
import cookbook.persistence.repository.CommentRepository;
import cookbook.persistence.repository.RecipeRepository;
import cookbook.service.cook.CookService;
import cookbook.service.dto.RecipeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CookbookCommentService implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private CookService cookService;

    @Override
    public void saveComment(RecipeDto recipe, String comment) {
        var r = recipeRepository.findById(recipe.getId()).orElse(null);
        commentRepository.save(new Comment(comment, r, (Cook)cookService.getCurrentUser()));
    }
}
