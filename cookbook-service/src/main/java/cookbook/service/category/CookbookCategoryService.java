package cookbook.service.category;

import cookbook.domain.Category;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class CookbookCategoryService implements CategoryService {

    private final Set<Category> categories = Set.of(Category.values());

    @Override
    public Set<Category> getCategories() {
        return categories;
    }
}
