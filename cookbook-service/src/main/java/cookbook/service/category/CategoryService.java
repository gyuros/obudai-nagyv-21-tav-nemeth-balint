package cookbook.service.category;

import cookbook.domain.Category;
import org.springframework.stereotype.Component;

import java.util.Set;

public interface CategoryService {

    Set<Category> getCategories();
}
