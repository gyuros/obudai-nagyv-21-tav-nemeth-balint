package cookbook.service.cook;

import cookbook.persistence.entity.User;
import cookbook.service.dto.CookDto;

import java.io.IOException;

public interface CookService {

    void logout() throws IOException;

    boolean isLoggedIn();

    User getCurrentUser();

    CookDto getCurrentCookDto();

    long getCurrentUserId();

    boolean authenticate(CookDto user);
}
