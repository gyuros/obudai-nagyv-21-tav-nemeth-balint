package cookbook.service.cook;

import cookbook.persistence.entity.User;
import cookbook.service.dto.UserPrincipal;
import cookbook.persistence.entity.Cook;
import cookbook.persistence.repository.CookRepository;
import cookbook.service.dto.CookDto;
import cookbook.service.transformer.CookTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CookbookCookService implements CookService, UserDetailsService {

    @Autowired
    private CookRepository cookRepository;

    @Autowired
    private CookTransformer transformer;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private CookDto currentUser;

    @Override
    public void logout() throws IOException {
        currentUser = null;
    }

    @Override
    public boolean isLoggedIn() {
        return currentUser != null;
    }

    @Override
    public User getCurrentUser() {
        var userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPrincipal.getUser();
    }

    @Override
    public CookDto getCurrentCookDto() {
        return transformer.toCookDto((Cook)getCurrentUser());
    }

    @Override
    public long getCurrentUserId() {
        return getCurrentUser().getId();
    }

    @Override
    public boolean authenticate(CookDto user) {
        var cook = cookRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());

        if(cook != null)
            currentUser = transformer.toCookDto(cook);

        return cook != null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Cook cook = cookRepository.findByUsername(username);
        if(cook == null)
            throw new UsernameNotFoundException(username);

        return new UserPrincipal(cook);
    }
}
