package cookbook.service.recipe;

import com.google.common.collect.Lists;
import cookbook.exception.ModelNotFoundException;
import cookbook.persistence.repository.CookRepository;
import cookbook.persistence.repository.RecipeRepository;
import cookbook.service.cook.CookService;
import cookbook.service.dto.RecipeDto;
import cookbook.service.transformer.RecipeTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CookbookRecipeService implements RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private CookRepository cookRepository;

    @Autowired
    private RecipeTransformer transformer;

    @Autowired
    private CookService cookService;

    @Override
    public void addRecipe(RecipeDto recipe) {
        var saved = recipeRepository.save(transformer.toRecipe(recipe));
        recipe.setId(saved.getId());
    }

    @Override
    @Transactional
    public List<RecipeDto> getRecipes() {
        return transformer.toRecipeDtos(Lists.newArrayList(recipeRepository.findAll()));
    }

    @Override
    public void deleteRecipe(long id) throws ModelNotFoundException {
        var removable = recipeRepository.findById(id);


        if(!removable.isPresent() || getRecipesByUserId(cookService.getCurrentUserId()).contains(removable))
            throw new ModelNotFoundException("Model not found.");

        recipeRepository.delete(removable.get());
    }

    @Override
    public RecipeDto findById(long id) {
        var recipe = recipeRepository.findById(id);
        if(recipe.isEmpty())
            throw new ModelNotFoundException(String.format("Recipe with id %s not found.", id));

        return transformer.toRecipeDto(recipe.get());
    }

    @Override
    public List<RecipeDto> getRecipesByUserId(long userId) {
        return getRecipes().stream().filter(x -> x.getUploader().getId() == userId).collect(Collectors.toList());
    }
}
