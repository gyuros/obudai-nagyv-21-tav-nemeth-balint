package cookbook.service.recipe;

import cookbook.exception.ModelNotFoundException;
import cookbook.persistence.entity.User;
import cookbook.service.dto.RecipeDto;

import java.util.List;

public interface RecipeService {

    void addRecipe(RecipeDto recipe);

    List<RecipeDto> getRecipes();

    void deleteRecipe(long id) throws ModelNotFoundException;

    RecipeDto findById(long id);

    List<RecipeDto> getRecipesByUserId(long userId);
}
