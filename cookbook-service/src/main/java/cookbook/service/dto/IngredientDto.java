package cookbook.service.dto;

import cookbook.domain.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class IngredientDto {

    private double amount;
    private String name;
    private Unit unit;

    public IngredientDto(double amount, String name, Unit unit) {
        this.amount = amount;
        this.name = name;
        this.unit = unit;
    }

    public static List<IngredientDto> parse(String ingredients) {
        var ingredientDtos = new ArrayList<IngredientDto>();
        if(ingredients == null || ingredients.equals(""))
            return ingredientDtos;

        var lines = ingredients.split("\r\n");

        for (var line : lines) {
            var words = line.split(" ");
            var amount = Double.valueOf(words[0]);
            var unit = Unit.valueOf(words[1].toUpperCase(Locale.ROOT));
            var name = words[2];
            ingredientDtos.add(new IngredientDto(amount, name, unit));
        }
        return ingredientDtos;
    }

    public double getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public Unit getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", amount, unit, name);
    }
}
