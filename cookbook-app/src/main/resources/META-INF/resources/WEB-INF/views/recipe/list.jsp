<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cookbook - Recipes</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">Cookbook</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/recipes">Recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/my-recipes">My recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/recipes/create">New recipe</a>
                </li>
            </ul>
            <form:form class="form-inline my-2 my-lg-0" action="/logout">
                <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
            </form:form>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row mt-5">
        <form:form cssClass="col-12">
            <div class="row">
                <div class="form-inline my-2 my-lg-0 m-auto">
                    <input class="form-control mr-sm-2" type="search" name="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </div>
            </div>
            <div class="row">
                <c:forEach var="searchType" items="${searchTypes}">
                    <div class="m-auto">
                        <input type="checkbox" name="types[]" value="${searchType}"> ${searchType}
                    </div>
                </c:forEach>
            </div>
        </form:form>
    </div>



    <div class="row">
        <h1 class="mt-5 w-100 text-center">Recipes</h1>
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Categories</th>
                <th scope="col">Servings</th>
                <th scope="col">Uploader</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="recipe" items="${recipes}">
                <tr>
                    <td><a class="text-light" href="/recipes/${recipe.getId()}">${recipe.getName()}</a></td>
                    <td>${recipe.getCategories().toString()}</td>
                    <td>${recipe.getServings()}</td>
                    <td>${recipe.getUploader().getUsername()}</td>
                </tr>
            </c:forEach>
    </div>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>