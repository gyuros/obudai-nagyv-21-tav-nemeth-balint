<!DOCTYPE html>
<html lang="en">
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <meta charset="UTF-8">
    <title>Cookbook - Error</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
</head>
<body>

<div class="text-center">
    <h1 class="text-danger">Error</h1>
    <h2>Message: ${errorMessage}</h2>
</div>


</body>
</html>