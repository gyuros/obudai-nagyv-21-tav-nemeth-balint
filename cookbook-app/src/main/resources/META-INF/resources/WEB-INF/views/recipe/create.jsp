<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cookbook - Create recipe</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">Cookbook</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/recipes">Recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/my-recipes">My recipes</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/recipes/create">New recipe</a>
                </li>
            </ul>
            <form:form class="form-inline my-2 my-lg-0" action="/logout">
                <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
            </form:form>
        </div>
    </div>
</nav>

<div class="container">
    <div class="card mt-5">
        <div class="card-body">
            <h5 class="card-title">New recipe</h5>

            <c:forEach var="error" items="${errors}">
                <div class="text-center">
                    <span class="text-danger">${error}</span>
                </div>
            </c:forEach>

            <form:form modelAttribute="request" class="my-2 my-lg-0" action="/recipes/create" method="post">

                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="text-dark col-2">Name</label>
                                <form:input type="text" name="name" id="name" class="form-control col-10" path="name" />
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="servings" class="text-dark col-2">Servings</label>
                                <form:input type="number" value="1" name="servings" id="servings" class="form-control col-10" path="servings" />
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="ingredients" class="text-dark col-2">Ingredients</label>
                                <form:textarea type="text" rows="4" name="ingredients" id="ingredients" class="form-control col-10" path="ingredients"></form:textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="preparation" class="text-dark col-2">Preparation</label>
                                <form:textarea type="text" rows="4" name="preparation" id="preparation" class="form-control col-10" path="preparation"></form:textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="categories" class="text-dark col-2">Categories</label>
                                <form:textarea type="text" rows="4" name="categories" id="categories" class="form-control col-10" path="categories"></form:textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>

                </div>

            </form:form>
        </div>
    </div>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>