<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cookbook - ${recipe.getName()}</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">Cookbook</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/recipes">Recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/my-recipes">My recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/recipes/create">New recipe</a>
                </li>
            </ul>
            <form:form class="form-inline my-2 my-lg-0" action="/logout">
                <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
            </form:form>
        </div>
    </div>
</nav>

<div class="container">
    <div class="card mt-5">
        <div class="card-body">
            <h5 class="card-title">${recipe.getName()}</h5>
            <table class="table">
                <tr>
                    <td><strong>Preparation</strong></td>
                    <td>${recipe.getPreparation()}</td>
                </tr>
                <tr>
                    <td><strong>Ingredients</strong></td>
                    <td>
                        <c:forEach var="ingredient" items="${recipe.getIngredients()}">
                            ${ingredient}<br>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td><strong>Categories</strong></td>
                    <td>${fn:join(recipe.getCategories().stream().map(x -> x.name()).toArray(), ', ')}</td>
                </tr>
                <tr>
                    <td><strong>Servings</strong></td>
                    <td>${recipe.getServings()}</td>
                </tr>
                <tr>
                    <td><strong>Uploader</strong></td>
                    <td>${recipe.getUploader().getUsername()}</td>
                </tr>
            </table>

            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">Comments</h5>
                    <table class="table">
                        <tr>
                            <td><strong>Comment</strong></td>
                            <td><strong>Time</strong></td>
                        </tr>
                        <c:forEach var="comment" items="${recipe.getComments()}">
                            <tr>
                                <td>${comment.getDescription()}</td>
                                <td>${comment.getTimestamp()}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

            <form:form action="/comment/create" method="post">
                <input type="hidden" name="recipeId" value="${recipe.getId()}">
                <div class="row">
                    <div class="col-offset-3 form-inline my-2 my-lg-0 m-auto">
                        <input class="form-control mr-sm-2" type="search" name="text" placeholder="Comment" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Comment</button>
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>