package cookbook.model;

public class CreateCommentRequest {
    private long recipeId;
    private String text;

    public long getRecipeId() {
        return recipeId;
    }

    public String getText() {
        return text;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    public void setText(String text) {
        this.text = text;
    }
}
