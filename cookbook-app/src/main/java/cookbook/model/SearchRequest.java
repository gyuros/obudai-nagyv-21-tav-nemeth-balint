package cookbook.model;

import cookbook.service.search.SearchModel;

import java.util.List;

public class SearchRequest implements SearchModel {
    private String text;
    private List<String> types;

    public String getText() {
        return text;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
