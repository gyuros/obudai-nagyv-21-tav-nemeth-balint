package cookbook.controller;

import cookbook.model.CreateCommentRequest;
import cookbook.service.comment.CommentService;
import cookbook.service.recipe.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private RecipeService recipeService;

    @PostMapping("/comment/create")
    public String create(CreateCommentRequest request, Model model) {
        var recipe = recipeService.findById(request.getRecipeId());
        commentService.saveComment(recipe, request.getText());
        return "redirect:/recipes/" + request.getRecipeId();
    }
}
