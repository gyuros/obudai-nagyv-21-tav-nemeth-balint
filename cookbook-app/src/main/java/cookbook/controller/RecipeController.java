package cookbook.controller;

import cookbook.domain.Category;
import cookbook.model.CreateRecipeRequest;
import cookbook.model.SearchRequest;
import cookbook.service.category.CategoryService;
import cookbook.service.cook.CookService;
import cookbook.service.dto.IngredientDto;
import cookbook.service.dto.RecipeDto;
import cookbook.service.recipe.RecipeService;
import cookbook.service.search.SearchService;
import cookbook.service.transformer.CookTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class RecipeController {

    private final List<String> searchTypes = List.of("name", "category", "ingredient", "uploader");

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private CookService cookService;

    @Autowired
    private CookTransformer cookTransformer;

    @GetMapping("/")
    public String home(Model model) {
        return "redirect:recipes";
    }

    @GetMapping("/recipes")
    public String recipes(Model model) {
        model.addAttribute("recipes", recipeService.getRecipes());
        model.addAttribute("searchTypes", searchTypes);
        return "recipe/list";
    }

    @PostMapping("/recipes")
    public String recipes(SearchRequest request, Model model) {
        model.addAttribute("recipes", searchService.filter(recipeService.getRecipes(), request));
        model.addAttribute("searchTypes", searchTypes);
        return "recipe/list";
    }

    @GetMapping("/recipes/create")
    public String create(Model model) {
        model.addAttribute("request", new CreateRecipeRequest());
        return "recipe/create";
    }

    @GetMapping("/my-recipes")
    public String myRecipes(Model model) {
        var recipes = recipeService.getRecipesByUserId(cookService.getCurrentUserId());
        model.addAttribute("recipes", recipes);
        return "recipe/my";
    }

    @GetMapping("/recipes/{id}")
    public String details(Model model, @PathVariable long id) {
        model.addAttribute("recipe", recipeService.findById(id));
        return "recipe/details";
    }

    @GetMapping("/recipes/delete/{id}")
    public String delete(Model model, @PathVariable long id) {
        recipeService.deleteRecipe(id);
        return "redirect:/my-recipes";
    }

    @PostMapping("/recipes/create")
    public String create(CreateRecipeRequest request, BindingResult bindingResult, Model model) {
        var errors = new ArrayList<String>();
        List<Category> categories = null;
        List<IngredientDto> ingredients = null;
        if (request.getName() == null || request.getName().equals("")) {
            errors.add("Name is required.");
        }

        try {
            ingredients = IngredientDto.parse(request.getIngredients());
        } catch (Exception e) {
            errors.add("Ingredient format error.");
        }

        try {
            categories = Category.parse(request.getCategories());
        } catch (Exception e) {
            errors.add("Categories format error.");
        }

        model.addAttribute("request", request);
        if(!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            return "/recipe/create";
        }

        var recipe = new RecipeDto(request.getName(), request.getServings(), request.getPreparation(), cookService.getCurrentCookDto(), categories, ingredients);
        recipeService.addRecipe(recipe);

        return "redirect:/my-recipes";
    }

}
