package cookbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class CookbookApplication {
    public static void main(String[] args) {
        SpringApplication.run(CookbookApplication.class, args);
    }
}
