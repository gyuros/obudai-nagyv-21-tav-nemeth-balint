package cookbook.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public enum Category {
    SALTY,
    SWEET,
    SOUP,
    MAIN_COURSE,
    DESSERT;

    public static List<Category> parse(String categories) {
        var categoryList = new ArrayList<Category>();
        if(categories == null || categories.equals(""))
            return categoryList;

        var lines = categories.split("\r\n");

        for (var line : lines) {
            var c = Category.valueOf(line.toUpperCase(Locale.ROOT));
            categoryList.add(c);
        }
        return categoryList;
    }
}
